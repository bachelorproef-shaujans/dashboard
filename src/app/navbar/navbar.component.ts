import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../shared/authentication.service';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  user: Observable<firebase.User>;

  constructor(
    public router: Router,
    public authService: AuthenticationService
  ) {
    document.querySelector('body').classList.remove('bg--blue');
  }

  ngOnInit() {
    this.user = this.authService.authUser();
    console.log(this.user);
  }

  logout() {
    this.authService.logout().then(onResolve => this.router.navigate(['/login']));
  }

}
