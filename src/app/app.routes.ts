import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

import { AuthguardService } from './shared/authguard.service';

import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProjectViewComponent } from './project-view/project-view.component';

const appRoutes: Routes = [
  {path: '', component: AppComponent, children: [
    {path: 'login', component: LoginComponent},
    {path: '', component: NavbarComponent, canActivate: [AuthguardService], children: [
      {path: 'projects', component: ProjectViewComponent, canActivate: [AuthguardService]},
    ]}
  ]},
];

export const routes = RouterModule.forRoot(appRoutes, {useHash: false});
