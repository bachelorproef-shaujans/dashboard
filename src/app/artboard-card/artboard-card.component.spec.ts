import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtboardCardComponent } from './artboard-card.component';

describe('ArtboardCardComponent', () => {
  let component: ArtboardCardComponent;
  let fixture: ComponentFixture<ArtboardCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtboardCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtboardCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
