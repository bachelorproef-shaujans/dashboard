import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { environment } from '../environments/environment';
import { routes } from './app.routes';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AuthenticationService } from './shared/authentication.service';
import { AuthguardService } from './shared/authguard.service';
import { ProjectService } from './shared/project.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ProjectViewComponent } from './project-view/project-view.component';
import { ProjectCardComponent } from './project-card/project-card.component';
import { ProjectFormComponent } from './project-form/project-form.component';
import { PageCardComponent } from './page-card/page-card.component';
import { ArtboardCardComponent } from './artboard-card/artboard-card.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { PageAndArtboardCountPipe } from './shared/page-and-artboard-count.pipe';
import { TimeAgoPipe } from './shared/time-ago.pipe';
import { HashcolorPipe } from './shared/hashcolor.pipe';
import { KeysPipe } from './shared/keys.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProjectViewComponent,
    ProjectListComponent,
    ProjectCardComponent,
    ProjectFormComponent,
    PageCardComponent,
    ArtboardCardComponent,
    NavbarComponent,
    PageAndArtboardCountPipe,
    TimeAgoPipe,
    HashcolorPipe,
    KeysPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routes,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
  ],
  providers: [AuthenticationService, AuthguardService, ProjectService],
  bootstrap: [AppComponent]
})
export class AppModule { }
