import { Component, OnInit } from '@angular/core';
import { Project } from '../shared/project.model';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.scss']
})
export class ProjectViewComponent implements OnInit {

  selectedProject: Project;
  addBool: boolean;
  hideList: boolean;

  constructor() { }

  ngOnInit() {
  }

  getSelectedProject(event) {
    this.selectedProject = event;
  }

  addOrUpdate(event) {
    this.addBool = event;
  }

  showOrHide(event) {
    this.hideList = event;
  }
}
