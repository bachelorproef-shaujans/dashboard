import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AuthenticationService } from '../shared/authentication.service';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  message: string;

  constructor(
    public router: Router,
    public authService: AuthenticationService
  ) {
    document.querySelector('body').classList.add('bg--blue');
  }

  loginWithGoogle() {
    this.authService.loginWithGoogle()
      .then(resolve => this.router.navigate(['projects']))
      .catch(error => this.message = error.message)
  }

  loginWithGithub() {
    this.authService.loginWithGithub()
      .then(resolve => this.router.navigate(['projects']))
      .catch(error => this.message = error.message)
  }
}
