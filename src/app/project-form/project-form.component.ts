import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProjectService } from '../shared/project.service';
import { Project } from '../shared/project.model';

@Component({
  selector: 'app-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.scss']
})
export class ProjectFormComponent implements OnInit {

  @Input() project: Project;
  @Input() addProject: boolean;

  @Output() hideList = new EventEmitter<boolean>();

  loading: boolean;
  showUpload: boolean = false;

  constructor(
    public projectService: ProjectService
  ) { }

  ngOnInit() { }

  log3(value) {
    console.log(this.projectService.currentDesignImages[value]);

  }

  newProject() {
    this.loading = true;
    let file: File;
    for (let selectedFile of [(<HTMLInputElement>document.getElementById('file')).files[0]]) {
      file = selectedFile;
    }
    this.projectService.addProject(this.project, file).then(result => { this.loading = false; this.closeForm() });
  }

  updateProject() {
    this.loading = true;
    let file: File;
    for (let selectedFile of [(<HTMLInputElement>document.getElementById('file')).files[0]]) {
      file = selectedFile;
    }
    this.projectService.updateProduct(this.project, file).then(result => { this.loading = false; });
  }

  deleteProject() {
    this.projectService.deleteProject(this.project);
    this.project = null;
    this.hideList.emit(false);
  }

  closeForm() {
    this.project = null;
    this.hideList.emit(false);
  }

}
