import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ProjectService } from '../shared/project.service';
import { Project } from '../shared/project.model';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {

  projects: Observable<Project[]>;

  @Output() selectProject = new EventEmitter<Project>();
  @Output() add = new EventEmitter<boolean>();
  @Output() hideList = new EventEmitter<boolean>();

  constructor(
    public projectService: ProjectService
  ) { }

  ngOnInit() {
    this.projects = this.projectService.getProjects();
  }

  selectedProject(project: Project) {
    console.log(project);
    this.projectService.getProjectDetails(project);
    this.selectProject.emit(project);
    this.add.emit(false);
    this.hideList.emit(true);
  }

  initializeNew() {
    this.selectProject.emit(this.projectService.initializeNew());
    this.add.emit(true);
    this.hideList.emit(true);
  }

  niceWord(i) {
    let items = ['wonderfull', 'beautifull', 'pretty', 'gorgeous'];

    if (i >= items.length) {
      i = i % items.length;
    }

    return items[i];
  }

}
