import {Injectable, Inject} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Project} from './project.model';

import {FirebaseApp} from 'angularfire2';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFireDatabase} from 'angularfire2/database';
import 'firebase/storage';

import * as JSZip from 'jszip';
import * as JSZipUtils from 'jszip-utils';

import 'rxjs/add/operator/mergeMap';

@Injectable()
export class ProjectService {
  private uid: string;
  private firebasestorage: any;
  public currentDesignImages: any = [];

  initializeNew(): Project {
    return {name: '', description: '', meta: '', designUrl: '', created: Date.now(), updated: Date.now()};
  }

  constructor(public afAuth: AngularFireAuth,
              public afd: AngularFireDatabase,
              public fbApp: FirebaseApp) {
    this.afAuth.authState.subscribe(auth => {
        if (auth !== undefined && auth !== null) {
          this.uid = auth.uid;
        }
      }
    );

    this.firebasestorage = fbApp.storage();
  }

  getProjects(): Observable<Project[]> {
    return this.afd.list('projects/' + this.uid);
  }

  addProject(project: Project, file: File) {
    const zipFile = new JSZip();
    const self = this;

    if (this.uid !== undefined && this.uid !== null) {
      let key = this.afd.list('projects/' + this.uid).$ref.ref.push().key;

      zipFile.loadAsync(file).then(function (zip) {
        zip.forEach(function (relativePath, zipEntry) {
          if (relativePath === 'data.js') {
            zipEntry.async('string').then(function success(content) {
              project.meta = JSON.parse(content);
            })
          } else {
            // Save all artboards as files FAIL
            // if (zipEntry.name.endsWith('artboard.png')) {
            //   let filename = zipEntry.name.slice(0, zipEntry.name.indexOf('/'));
            //   zipEntry.async('blob').then(function success(content) {
            //     this.firebasestorage.ref(`projects/` + this.uid + `/` + key + `/` + filename + `.png`).put(content)
            //       .then((snapshot) => {
            //         console.log(snapshot);
            //       });
            //   })
            // }
          }
        })
      })

      return this.firebasestorage.ref(`projects/` + this.uid + `/` + key + `/` + project.name).put(file).then(
        snapshot => {
          project.designUrl = snapshot.downloadURL;
          this.afd.object('projects/' + this.uid + '/' + key).set(project);
        });
    }
  }

  updateProduct(cas: Project, file: File) {
    if (this.uid !== undefined && this.uid !== null) {
      if (file !== undefined && file !== null) {
        return this.firebasestorage.ref(`projects/` + this.uid + `/` + cas.$key + `/` + file.name).put(file).then(
          snapshot => {
            cas.designUrl = snapshot.downloadURL;
            this.afd.object('projects/' + this.uid + '/' + cas.$key).update({
              name: cas.name,
              description: cas.description,
              meta: cas.meta,
              updated: Date.now(),
              designUrl: cas.designUrl
            });
          });
      } else {
        return this.afd.object('projects/' + this.uid + '/' + cas.$key).update({
          name: cas.name,
          description: cas.description,
          meta: cas.meta,
          updated: Date.now(),
          designUrl: cas.designUrl
        });
      }
    }
  }

  deleteProject(cas: Project) {
    if (this.uid !== undefined && this.uid !== null) {
      return this.afd.list('projects').remove(cas.$key).then(
        onResolve => {
          this.firebasestorage.ref(`projects/` + this.uid + `/` + cas.$key + `/` + cas.name).delete().then(
            snapshot => {
              console.log(snapshot.downloadURL);
            });
        }
      );
    }
  }

  getProjectDetails(project: Project) {
    let self = this;
    let zipFile = new JSZip();
    let designUrl = project.designUrl;
    let meta = project.meta;

    JSZipUtils.getBinaryContent(designUrl, function(err, data) {
      if (err) { console.error(err); return; }
      for (let pageId in meta.pageOrder) {
        if (meta.pageOrder.hasOwnProperty(pageId)) {
          let page = meta.pageData[meta.pageOrder[pageId]];
          if (page.name !== 'Symbols') {
            for (let artboard in page.artboardId) {
              try {
                zipFile.loadAsync(data)
                  .then(function(zip) {
                    return zip.file(page.artboardId[artboard] + '/artboard-small.png').async('base64');
                  })
                  .then(function success(content) {
                    self.currentDesignImages[page.artboardId[artboard]] = { 'image': 'data:image/png;base64,' + content };
                    let image = page.artboardId[artboard];
                    let artboardImage = document.querySelector('[data-artboard="' + image + '"]');
                    artboardImage.setAttribute( 'src', 'data:image/png;base64,' + content );
                  }, function error(e) {
                    console.error(e);
                  })
              } catch (e) {
                console.error(e);
              }
            }
          }
        }
      }
    })
  }

  getProject(project: Project) {
    let pages = [];
    for (let pageId in project.meta.pageOrder) {
      if (project.meta.pageOrder.hasOwnProperty(pageId)) {
        let page = project.meta.pageData[project.meta.pageOrder[pageId]];

        if (page.name !== 'Symbols') {
          pages.push(page);
        }
      }
    }
    return pages;
  }
}
