export interface Project {
  $key?: string;
  name: string;
  description?: string;
  meta?: any;
  designUrl: string;
  created: number;
  updated: number;
}
