import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pageAndArtboardCount'
})
export class PageAndArtboardCountPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let pageCount = 0;
    let artboardCount = 0;

    for (let page in value) {
      if (value.hasOwnProperty(page)) {
        pageCount++;
        for (let artboard in value[page]['artboardId']) {
          artboardCount++;
        }
      }
    }

    // for (let page in value) {
    //   pageCount++;
    //
    //   console.log(page.name);
    //
    //   if (page.hasOwnProperty('artboardId')) {
    //     console.log(page);
    //   }
    // }

    return pageCount + ' pages and ' + artboardCount +  ' artboards';
  }

}
